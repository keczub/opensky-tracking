import requests
import plotly.graph_objects as go
# https://geographiclib.sourceforge.io/1.50/python/examples.html#basic-geodesic-calculations
from geographiclib.geodesic import Geodesic
import geocoder
from typing import TypeVar, Tuple, List
UserClass = TypeVar('User')
StateClass = TypeVar('State')
PlotlyFigure = TypeVar('plotly.figure')

class User:
    
    def __init__(self, longitude: float, latitude: float) -> None:
        self.longitude = longitude
        self.latitude = latitude
        self.rgb_color = "rgba({0},{1},{2},{3})".format(243, 189, 61, 1)
        
class State:
        
    def __init__(self, raw_state: list) -> None:
        
        # Information from response
        self.icao24, self.callsign, self.origin_country, self.time_position,\
        self.last_contact, self.longitude, self.latitude, self.geo_altitude, \
        self.on_ground, self.velocity, self.heading, self.vertical_rate, \
        self.sensors, self.baro_altitude, self.squawk, self.spi, \
        self.position_source =  self.read_json(raw_state)
        
        # Position related to User
        self.related_distance, self.related_azimuth = None, None
           
        # Calculate direction vector for visualization
        self.direction_vector = self.calcaulateDirection()
        
        # Create rgb color 
        self.rgba_color = "rgba({0},{1},{2},{3})".format(153, 153, 255, 1)
             
    def read_json(self, raw_state: list) -> list:        
        return [raw_state[i] for i in range(17)]
        
    def calcaulateReference(self, User: UserClass) -> None:
        
        # Calculating distance and angle between user and plane to plot on radar
        geod = Geodesic.WGS84
        reference_dict = geod.Inverse(User.latitude, User.longitude, self.latitude, self.longitude)
        if reference_dict['azi1'] > 0:
            self.related_azimuth = reference_dict['azi1']
        else:
            self.related_azimuth = reference_dict['azi1'] + 360
        self.related_distance = reference_dict['s12']
    
    def calcaulateDirection(self):
        try:
            # Calculating direction requires angle modification to set geographic north as a beggining
            # Velocity multiplayer helps visualize speed on map
            geod = Geodesic.WGS84
            geod_dict = geod.Direct(self.longitude, self.latitude, -self.heading+90, 20000)
            return [[geod_dict['lat1'], geod_dict['lat2']], [geod_dict['lon1'], geod_dict['lon2']]]
        except:
            return None
          
class Interval:

    def __init__(self, User: UserClass, number_of_states_list, state_time_list):        
        self.last_response = self.makeRequest(User)
        self.states_list = []
        self.extractStatesFromResponse(User)
        
        # Create plots based only on interval data
        self.plots = Plots()
        self.map = self.plots.drawMap(User, self.states_list)
        self.table = self.plots.createTable(self.states_list)
        self.radar = self.plots.createRadar(self.states_list)
        
        # Plot based on cached values form other interval
        self.number_of_states_list, self.state_time_list = self.createListsForBarPlot(number_of_states_list, state_time_list) 
        self.bar = self.drawBarPlot(self.number_of_states_list, self.state_time_list)

    def extractStatesFromResponse(self, User):
        for raw_state in self.last_response:
            self.states_list.append(State(raw_state))
        [i.calcaulateReference(User) for i in self.states_list]

    def createListsForBarPlot(self, number_of_states_list: List[int], state_time_list: List[int]) -> Tuple[List[int], List[int]]:
    
        # List of 20 last values of amount of aircrafts for interval
        
        if len(number_of_states_list) > 20:
            number_of_states_list.pop(0)
            number_of_states_list.append(len([i for i in self.states_list]))
        else:
            number_of_states_list.append(len([i for i in self.states_list]))
        
        if len(number_of_states_list) > 20:
            state_time_list.pop(0)
            state_time_list.append(state_time_list[-1]+1)
        else:
            state_time_list.append(state_time_list[-1]+1)
        
        return number_of_states_list, state_time_list
             
    def makeRequest(self, user: UserClass) -> list:
        
        # Extent multiplayer. Only for development
        d = 2
        
        lamin = user.latitude - 0.5*d
        lomin = user.longitude - 1.0*d
        lamax = user.latitude + 0.5*d
        lomax = user.longitude + 1.0*d
        r = requests.get("https://opensky-network.org/api/states/all?lamin={0}&lomin={1}&lamax={2}&lomax={3}"
                         .format(lamin, lomin, lamax, lomax))
        return r.json()['states']
        
    @staticmethod
    def drawBarPlot(states_list, lista_time) -> PlotlyFigure:

        if (states_list is None) and (lista_time is None):
                # number of aircrafts on map
                fig = go.Figure(data=[go.Bar(name="amount", x=[None], y=[None])])
                fig.update_traces(marker_color='rgb(122, 215, 183)', marker_line_color='rgb(122, 215, 183)',
                                  marker_line_width=1.5, opacity=0.8)
                fig.update_layout(margin=dict(l=30, r=30, t=100, b=100))
                fig.update_xaxes(title_text='N-th request')
                fig.update_yaxes(title_text='Number of Aircraft states <br> in request response',
                                 title_font=dict(size=14))
        else:
                # number of aircrafts on map
                fig = go.Figure(data=[go.Bar(name="amount", x=lista_time, y=states_list)])
                fig.update_traces(marker_color='rgb(122, 215, 183)', marker_line_color='rgb(122, 215, 183)',
                                  marker_line_width=1.5, opacity=0.8)
                fig.update_layout(margin=dict(l=30, r=30, t=100, b=100))
                fig.update_xaxes(title_text='N-th request')
                fig.update_yaxes(title_text='Number of Aircraft states <br> in request response',
                                 title_font=dict(size=14))
      
        return fig
 
class Plots:
    def __init__(self):
        # Plots general variables
        self.mapbox_access_token = open(".mapbox_token").read()
        self.first_distance_threshold = 50000
        self.first_rgb_color = "rgba({0},{1},{2},{3})".format(132, 73, 174, 1)
        self.second_distance_threshold = 150000
        self.second_rgb_color = "rgba({0},{1},{2},{3})".format(83, 133, 72, 1)
              
    def drawMap(self, user, states_list) -> PlotlyFigure:
        fig = go.Figure()
        geod = Geodesic.WGS84
        if (user is None) and (states_list is None):
            fig.add_trace(go.Scattermapbox(
                    lat=[None],
                    lon=[None],
                    mode='markers',
                    name="User",
                    marker=go.scattermapbox.Marker(
                        size=14,
                        color= "rgba({0},{1},{2},{3})".format(243, 189, 61, 1)),
                    text=['User']))

            fig.add_trace(go.Scattermapbox(
                    lat=[None],
                    lon=[None],
                    mode = "markers",
                    name="50 km",
                    marker=go.scattermapbox.Marker(
                        size=5,
                        color=self.first_rgb_color),
                    hoverinfo='none'))

            fig.add_trace(go.Scattermapbox(
                    lat=[None],
                    lon=[None],
                    mode = "markers",
                    name="50 km",
                    marker=go.scattermapbox.Marker(
                        size=5,
                        color= self.second_rgb_color),
                    hoverinfo='none'))

            fig.add_trace(go.Scattermapbox(
                        lat=[None],
                        lon=[None],
                        mode = "lines",
                        legendgroup="group1",
                        name='Aircraft directions',
                        marker=go.scattermapbox.Marker(
                            size=10,
                            color="rgba({0},{1},{2},{3})".format(153, 153, 255, 1)),
                        hoverinfo='none'))

            fig.add_trace(go.Scattermapbox(
                        lat=[None],
                        lon=[None],
                        mode='markers',
                        legendgroup="group2",
                        name='Aircraft positions',
                        marker=go.scattermapbox.Marker(
                            size=10,
                            color="rgba({0},{1},{2},{3})".format(153, 153, 255, 1)),
                        hoverinfo='text'))

            fig.update_layout(
                margin=dict(l=20*2, r=20*2, t=0, b=0),
                hovermode='closest',
                legend_orientation="h",
                mapbox=dict(
                    accesstoken=self.mapbox_access_token,
                    bearing=0,
                    center=go.layout.mapbox.Center(
                        lat=0,
                        lon=0),
                    pitch=0,
                    zoom=4))

        else:
            fig.add_trace(go.Scattermapbox(
                    lat=[user.latitude],
                    lon=[user.longitude],
                    mode='markers',
                    name="User",
                    marker=go.scattermapbox.Marker(
                        size=14,
                        color= user.rgb_color),
                    text=['User']))

            fig.add_trace(go.Scattermapbox(
                    lat=[j['lat2'] for j in [geod.Direct(user.latitude, user.longitude, i, self.first_distance_threshold) for i in list(range(0, 361, 15))]],
                    lon=[j['lon2'] for j in [geod.Direct(user.latitude, user.longitude, i, self.first_distance_threshold) for i in list(range(0, 361, 15))]],
                    mode = "markers",
                    name="50 km",
                    marker=go.scattermapbox.Marker(
                        size=5,
                        color=self.first_rgb_color),
                    hoverinfo='none'))

            fig.add_trace(go.Scattermapbox(
                    lat=[j['lat2'] for j in [geod.Direct(user.latitude, user.longitude, i, self.second_distance_threshold) for i in list(range(0, 361, 5))]],
                    lon=[j['lon2'] for j in [geod.Direct(user.latitude, user.longitude, i, self.second_distance_threshold) for i in list(range(0, 361, 5))]],
                    mode = "markers",
                    name="150 km",
                    marker=go.scattermapbox.Marker(
                        size=5,
                        color= self.second_rgb_color),
                    hoverinfo='none'))

            for number, state in enumerate(states_list):
                if number  == 0:
                    fig.add_trace(go.Scattermapbox(
                        lat=state.direction_vector[1],
                        lon=state.direction_vector[0],
                        mode = "lines",
                        legendgroup="group1",
                        name='Aircraft directions',
                        marker=go.scattermapbox.Marker(
                            size=10,
                            color=state.rgba_color),
                        hoverinfo='none'))

                    fig.add_trace(go.Scattermapbox(
                        lat=[state.latitude],
                        lon=[state.longitude],
                        mode='markers',
                        legendgroup="group2",
                        name='Aircraft positions',
                        marker=go.scattermapbox.Marker(
                            size=10,
                            color=state.rgba_color
                        ),
                        hoverinfo='text',
                        text=state.callsign,
                    ))
                else:
                    fig.add_trace(go.Scattermapbox(
                        lat=state.direction_vector[1],
                        lon=state.direction_vector[0],
                        mode = "lines",
                        legendgroup="group1",
                        marker=go.scattermapbox.Marker(
                            size=10,
                            color=state.rgba_color
                        ),
                        hoverinfo='none',
                        showlegend=False
                    ))

                    fig.add_trace(go.Scattermapbox(
                        lat=[state.latitude],
                        lon=[state.longitude],
                        mode='markers',
                        legendgroup="group2",
                        marker=go.scattermapbox.Marker(
                            size=10,
                            color=state.rgba_color
                        ),
                        hoverinfo='text',
                        text=state.callsign,
                        showlegend=False
                    ))

            fig.update_layout(
                margin=dict(l=20*2, r=20*2, t=0, b=0),
                hovermode='closest',
                legend_orientation="h",
                mapbox=dict(
                    accesstoken=self.mapbox_access_token,
                    bearing=0,
                    center=go.layout.mapbox.Center(
                        lat=user.latitude,
                        lon=user.longitude),
                    pitch=0,
                    zoom=6))

        return fig
               
    def createTable(self, states_list) -> PlotlyFigure:
        fig = go.Figure()
        if states_list is None:
            fig.add_trace(go.Table(
                                header=dict(values=[['Callsign'], ['icao24'], ['Origin country'], ['Geo altitude'],['On ground'],
                                                    ['Squawk'],['Velocity']],
                            fill_color="rgb(122, 215, 183, 1)",
                            align='center',
                            line_color='darkslategray',
                            font=dict(color='black', size=13))))
        else:
            fig.add_trace(go.Table(
                header=dict(values=[['Callsign'], ['icao24'], ['Origin country'], ['Geo altitude'],['On ground'],
                                    ['Squawk'],['Velocity']],
                            fill_color="rgb(122, 215, 183, 1)",
                            align='center',
                            line_color='darkslategray',
                            font=dict(color='black', size=13)),
                cells=dict(values=[[i.callsign for i in states_list],
                                     [i.icao24 for i in states_list],
                                     [i.origin_country for i in states_list],
                                     [i.geo_altitude for i in states_list],
                                     [i.on_ground for i in states_list],
                                     [i.squawk for i in states_list],
                                     [i.velocity for i in states_list]],
                           fill_color=["rgb(122, 215, 183, 1)",'white'],
                           line_color=['darkslategray','black'],
                           font=dict(color=['black', 'black'], size=12),
                           align='center')))

        return fig
        
    def createRadar(self, states_list) -> PlotlyFigure:
        fig = go.Figure()

        fig.add_trace(go.Scatterpolar(
                        r = [0],
                        theta = [0],
                                mode = 'markers',
                                name="User",
                                marker_color = "rgba({0},{1},{2},{3})".format(243, 189, 61, 1),
                                hoverinfo='none'))


        if states_list is None:
                fig.add_trace(go.Scatterpolar(
                                r = [self.first_distance_threshold/1000]*len((range(0, 361, 15))),
                                theta = list(range(0, 361, 15)),
                                mode = 'lines',
                                name="50 km",
                                line_dash = "dash",
                                line_color = self.first_rgb_color,
                                hoverinfo='none'))

                fig.add_trace(go.Scatterpolar(
                                r = [self.second_distance_threshold/1000]*len((range(0, 361, 15))),
                                theta = list(range(0, 361, 15)),
                                mode = 'lines',
                                name="150 km",
                                line_dash = "dash",
                                line_color = self.second_rgb_color,
                                hoverinfo='none'))


                fig.add_trace(go.Scatterpolar(r = [None],
                                        theta = [None],
                                        mode = 'markers',
                                        marker_color="rgba({0},{1},{2},{3})".format(153, 153, 255, 1),
                                        name="Aircraft",
                                        showlegend=True))

                fig.update_layout(margin=dict(l=40, r=40, t=40, b=40),
                             polar = dict(
                             radialaxis_angle = 90,
                             angularaxis = dict(
                             direction = "clockwise")))
        else:
        
                fig.add_trace(go.Scatterpolar(
                            r = [self.first_distance_threshold/1000]*len((range(0, 361, 15))),
                            theta = list(range(0, 361, 15)),
                            mode = 'lines',
                            name="50 km",
                            line_dash = "dash",
                            line_color = self.first_rgb_color,
                            hoverinfo='none'))

                fig.add_trace(go.Scatterpolar(
                            r = [self.second_distance_threshold/1000]*len((range(0, 361, 15))),
                            theta = list(range(0, 361, 15)),
                            mode = 'lines',
                            name="150 km",
                            line_dash = "dash",
                            line_color = self.second_rgb_color,
                            hoverinfo='none'))

                for state in states_list:
                    # related distance in meters, changed to km
                    if state.related_distance/1000 < self.second_distance_threshold/1000:
                            fig.add_trace(go.Scatterpolar(r = [round(state.related_distance/1000, 2)],
                                    theta = [state.related_azimuth],
                                    mode = 'markers',
                                    marker_color=state.rgba_color,
                                    name=state.callsign,
                                    showlegend=True))
                                
        fig.update_layout(margin=dict(l=40, r=40, t=40, b=40),
                         polar = dict(
                         radialaxis_angle = 90,
                         angularaxis = dict(
                         direction = "clockwise")))
                         
        return fig
 
class Main:
    
    def __init__(self, number_of_states_list, state_time_list, user_lat=53.963056, user_long=18.525833) -> None:
        
        # Starogard Gdansk geographic coordinates 18.525833, 53.963056
        self.user = User(user_long, user_lat)
        self.current_interval = Interval(self.user, number_of_states_list, state_time_list)

def geocode_text(text: str) -> str:
    response = geocoder.osm(text)
    if response.status_code == 200:
        try:
            lng = response.json['lng']
            lat = response.json['lat']
            address = response.current_result.address
            return (lng, lat), "Location set to: {0}".format(address)
        except:
            return (0.0, 0.0), "Geocoding for entered location failed."
    else:
        return (0.0, 0.0), "Geocoder status code: {0}".format(response.status_code)
