from libs.logic_library import Main, Plots, Interval, geocode_text
import plotly.graph_objects as go

# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

app = dash.Dash(__name__)

number_of_states_list=[0]*20
state_time_list = [i for i in range(-20, 0)]

obj_1 = Plots()
fig_2 = obj_1.createRadar(None)
fig_1 = obj_1.createTable(None)
fig_3 = obj_1.drawMap(None, None)
fig_4 = Interval.drawBarPlot(None, None)

app.layout = html.Div( children = [html.Div(id = "header-div",
                       children = html.H2(children = "What plane is now flying above me ? - OpenSky & Plotly powered plane radar dashboard", style = {'marginLeft': 10})),
                       html.Div(id = "control-bar-div", children=[
                       html.Div([html.H6(children = "Set up your location: ", style = {"display":"inline-block", 'marginLeft': 10, 'marginRight': 10}),
                                 dcc.Input(id = "location_input", value = "London", style = {"display":"inline-block"})], style = {"display":"inline-block"}),
                       html.Div([html.H6(children = " Current status: ", style = {"display":"inline-block",'marginLeft': 100, 'marginRight': 10}),
                                 dcc.RadioItems(options=[{'label':"Active", 'value':'', 'disabled':True}], value=None, id = "radio-btn", style = {"display":"inline-block"}),
                                 html.H6(children = "Updating: ", style = {"display":"inline-block", 'marginLeft': 100, 'marginRight': 10}),
                                 html.Button(children="Stop", id= "stop-btn", n_clicks=0, style = {"display":"inline-block"})], style = {"display":"inline-block"}),
                       html.H6(id="text_status", children = "", style = {'marginLeft': 10})]),

             html.Div([html.Div(id = 'map-div', children=dcc.Graph(id = 'map-update-plot', figure={'data': fig_3.data, 'layout': fig_3.layout},
                                          config={'scrollZoom': False, 'displayModeBar': False, 'doubleClick': False}), className='eight columns'),
                       html.Div(id = 'radar-div', children=dcc.Graph(id = 'radar-update-plot', figure={'data': fig_2.data, 'layout': fig_2.layout}), className='four columns')]),
             html.Div([html.Div(id = 'table-div', children=dcc.Graph(id = 'table-update-plot', figure={'data': fig_1.data, 'layout': fig_1.layout}), className='eight columns'),
                       html.Div(id = 'bar-div', children=dcc.Graph(id = 'bar-update-plot', figure={'data': fig_4.data, 'layout': fig_4.layout}), className='four columns')]),
                       dcc.Interval(id='interval-component', interval=11000, n_intervals=0, disabled=False),
                       dcc.Interval(id='interval-component-radiobtn', interval=550, n_intervals=0, disabled=False),
                       html.Div(id = "geocoded_text", hidden=True)])

# Multiple components can update everytime interval gets fired.
@app.callback([Output('radar-update-plot', 'figure'),
               Output('map-update-plot', 'figure'),
               Output('map-update-plot', 'config'),
               Output('table-update-plot', 'figure'),
               Output('bar-update-plot', 'figure')],
              [Input('interval-component', 'n_intervals'),
               Input('interval-component', "disabled"),
               Input("geocoded_text", "children")])
def update_graph_live(n, disabled, location_name):

    obj_1 = Main(number_of_states_list, state_time_list, float(location_name[1]), float(location_name[0]))

    fig_2 = obj_1.current_interval.radar
    fig_1 = obj_1.current_interval.table
    fig_3 = obj_1.current_interval.map
    fig_4 = obj_1.current_interval.bar

    if disabled == False:
        config={'scrollZoom': False,  'displayModeBar': False, 'doubleClick': False}
    else:
        config={'scrollZoom': True,  'displayModeBar': False, 'doubleClick': False}

    return fig_2, fig_3, config, fig_1, fig_4

@app.callback([Output("geocoded_text", "children"),
               Output("text_status", "children")],
              [Input('location_input', "value")])
def get_user_input(text):

    location_coords, location_name = geocode_text(text)
    return location_coords, location_name

@app.callback([Output("interval-component", "disabled"),
               Output("stop-btn", "children"),
               Output('interval-component-radiobtn', 'disabled'),
               Output('interval-component-radiobtn', "n_intervals")],
              [Input('stop-btn', "n_clicks")])
def stop_upd(n):

    if (n % 2 != 0) and (n != 0):
        return True, "Start", True, 0
    else:
        return False, "Stop", False, 2

@app.callback(Output("radio-btn", "value"),
              [Input('interval-component-radiobtn', "n_intervals")])
def radio_btn_upd(n: int):

    if n % 2 != 0:
        return ''
    else:
        return None

if __name__ == '__main__':
    app.run_server(debug=True)
